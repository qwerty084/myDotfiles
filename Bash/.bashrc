#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

function code() {
  cd
  ./VSCodium
}

export GPG_TTY=$(tty)

# aliases
alias nv='nvim'

