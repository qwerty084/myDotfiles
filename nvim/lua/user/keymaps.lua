local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

keymap('n', '<C-b>', ':NvimTreeToggle<CR>', opts)
keymap('n', '<C-space>', ':NvimTreeFocus<CR>', opts)
keymap('n', '<C-p>', ':Telescope find_files<CR>', opts)
keymap('n', '<c-s>', ':w<cr>', opts)
