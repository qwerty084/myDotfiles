local options = {
  backup = false,
  cmdheight = 1,
  termguicolors = true,
  expandtab = true,
  encoding = 'utf-8',
  fileencoding = 'utf-8',
  number = true,
  relativenumber = true,
  smartindent = true,
  tabstop = 2,
  shiftwidth = 2,
  smarttab = true,
  softtabstop = 2,
  expandtab = true,
  mouse = 'a',
  hidden = true,
  title = true,
  list = true,
  smartcase = true, -- new setting
  writebackup = false,
  updatetime = 300,
  showmode = false,
  scrolloff = 8, -- prev 999
}

vim.opt.shortmess:append "c"
vim.opt.listchars = {
  tab = ">·",
  trail = '·',
  extends = '>',
  precedes = '<',
}

for setting, value in pairs(options) do
  vim.opt[setting] = value
end
