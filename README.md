# My dotfiles...

---

## commands:

VSCodium
```
stow --target=${HOME}/.config/VSCodium/User/ User/
```

```
stow --target=${HOME}/.config targetDir
```

Code - OSS:

```
stow --target=${HOME}/.config/'Code - OSS'/User User/
```

alacritty:
```
stow --target=${HOME}/.config/alacritty/ alacritty/
```

xmobar:
```
stow --target=${HOME}/.config/xmobar/ xmobar/
```

Xmonad:
```
stow --target=${HOME}/ Xmonad/
```

Neovim:
```
stow --target=${HOME}/.config/nvim/ nvim/
```
